<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\Ad;
use App\Repository\AdRepository;

use App\Entity\Image;
use App\Repository\ImageRepository;

use App\Entity\Application;
use App\Repository\ApplicationRepository;

use Faker;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 20; $i++) {
            $ad = new Ad;
            $img = new Image;
            $app = new Application;

            $img->setUrl($faker->imageUrl('500', '500', 'people'));
            $img->setPath('une personne'); // alt

            $manager->persist($img);

            $ad->setTitle($faker->jobTitle);
            $ad->setAuthor($faker->userName()); //auteur
            $ad->setContent($faker->sentence(350)); //description
            $ad->setCreatedAt($faker->dateTime($max = 'now', $timezone = null)); //annonce crée le
            $ad->setImage($img);
            $ad->setPublished($faker->boolean);

            $app->setAuthor($ad->getAuthor());
            $app->setContent($ad->getContent());
            $app->setCreatedAt($ad->getCreatedAt());

            $manager->persist($app);

            $ad->setAdvert($app);

            $manager->persist($ad);
        }

        $manager->flush();
    }
}
