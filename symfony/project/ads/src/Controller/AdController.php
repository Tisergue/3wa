<?php

namespace App\Controller;

use Faker;
use App\Entity\Ad;
use App\Entity\Image;

use App\Entity\Application;

use App\Form\AdvertFormType;
use App\Repository\AdRepository;

use App\Repository\ImageRepository;
use App\Repository\ApplicationRepository;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class AdController extends AbstractController {
    /**
     * @Route("/", name="home_ad")
     */
    public function home() {
        $repo = $this->getDoctrine()->getRepository(Ad::class);
        $ad = $repo->findAllByDate();

        return $this->render('ad/home.html.twig', [
            'ads' => $ad
        ]);
    }

    /**
     * @Route("/accept", name="accept")
     */
    public function accept() {
        $repo = $this->getDoctrine()->getRepository(Ad::class);
        $ad = $repo->findAllByDate();

        return $this->render('ad/accept.html.twig', [
            'ads' => $ad
        ]);
    }

    /**
     * @Route("/del/{id}", name="del")
     */
    public function del($id) {
        $repo = $this->getDoctrine()->getManager();
        $ad = $repo->getRepository(Ad::class)->find($id);

        if (!$ad) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        $repo->remove($ad);
        $repo->flush();

        return $this->redirectToRoute('home_ad');
    }

    /**
     * @Route("/publish/{id}", name="publish")
     */
    public function publish($id) {
        $repo = $this->getDoctrine()->getManager();
        $ad = $repo->getRepository(Ad::class)->find($id);

        if (!$ad) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        $ad->setPublished(true);
        $repo->flush();

        return $this->redirectToRoute('home_ad');
    }

    /**
     * @Route("/apply/{id}", name="apply")
     */
    public function apply(Request $req, $id) {
        $app = new Application;

        $form = $this->createFormBuilder($app)
                     ->add('author')
                     ->add('content')
                     ->getForm();
                     
        $form->handleRequest($req);

        $manager = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {
            $repo = $this->getDoctrine()->getRepository(Ad::class);
            $ad = $repo->findOneById($id);

            $app->setCreatedAt(new \DateTime());
            $app->setAdvert($ad);

            $manager->persist($app);
            $manager->flush();

            return $this->redirectToRoute('home', ['page' => $id]);
        }

        return $this->render('ad/candidate.html.twig', [
            'formAd' => $form->createView()
        ]);
    }

    /**
     * @Route("/add", name="add")
     * @Route("/edit/{id}", name="edit")
     */
    public function management(Ad $ad = null, Request $req) {
        if (!$ad) {
            $faker = Faker\Factory::create();

            $ad     = new Ad;
            $img    = new Image;

            $img->setUrl($faker->imageUrl('500', '500', 'people'));
            $img->setPath('une personne'); // path représente alt en réalité !
        }

        $form = $this->createForm(AdvertFormType::class, $ad);

        $form->handleRequest($req);

        $manager = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$ad->getId()) {
                $ad->setCreatedAt(new \DateTime());
                $ad->setPublished(false);
                $ad->setImage($img);
            }

            $manager->persist($ad);
            $manager->flush();

            return $this->redirectToRoute('home_ad');
        }

        return $this->render('ad/management.html.twig', [
            'formAd' => $form->createView(),
            'isEdit' => $ad->getId() !== null
        ]);
    }

    /**
     * @Route("/{page}", name="home")
     */
    public function show($page) {
        $repo = $this->getDoctrine()->getRepository(Ad::class);
        $ad = $repo->findOneById($page);

        $repo = $this->getDoctrine()->getRepository(Application::class);
        $app = $repo->findByAdvertId($page);

        return $this->render('ad/show.html.twig', [
            'ad' => $ad,
            'applies' => $app
        ]);
    }
}
