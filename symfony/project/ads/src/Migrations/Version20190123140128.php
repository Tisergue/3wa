<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190123140128 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ad DROP FOREIGN KEY FK_77E0ED58D07ECCB6');
        $this->addSql('DROP INDEX IDX_77E0ED58D07ECCB6 ON ad');
        $this->addSql('ALTER TABLE ad DROP advert_id');
        $this->addSql('ALTER TABLE application ADD advert_id INT NOT NULL');
        $this->addSql('ALTER TABLE application ADD CONSTRAINT FK_A45BDDC1D07ECCB6 FOREIGN KEY (advert_id) REFERENCES ad (id)');
        $this->addSql('CREATE INDEX IDX_A45BDDC1D07ECCB6 ON application (advert_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ad ADD advert_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ad ADD CONSTRAINT FK_77E0ED58D07ECCB6 FOREIGN KEY (advert_id) REFERENCES ad (id)');
        $this->addSql('CREATE INDEX IDX_77E0ED58D07ECCB6 ON ad (advert_id)');
        $this->addSql('ALTER TABLE application DROP FOREIGN KEY FK_A45BDDC1D07ECCB6');
        $this->addSql('DROP INDEX IDX_A45BDDC1D07ECCB6 ON application');
        $this->addSql('ALTER TABLE application DROP advert_id');
    }
}
