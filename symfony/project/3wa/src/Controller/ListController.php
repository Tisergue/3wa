<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\UserList;
use App\Repository\UserListRepository;

class ListController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home(Request $resquest) {
        $search = $resquest->query->get('search');
        $repo = $this->getDoctrine()->getRepository(UserList::class);

        if ($search === null) {
            $user = $repo->findAll();
        } else {
            $user = $repo->findByYear($search);
        }

        return $this->render('list/index.html.twig', [
            'users' => $user
        ]);
    }

    /**
     * @Route("/user/{id}", name="show_user")
     */
    public function show($id) {
        $repo = $this->getDoctrine()->getRepository(UserList::class);
        $user = $repo->findOneById($id);

        return $this->render('list/show.html.twig', [
            'user' => $user
        ]);
    }
}
