<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\UserList;
use App\Repository\UserListRepository;

use Faker;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 200; $i++) {
            $user = new userList;

            $name = explode(' ', $faker->name);

            $user->setFname($name[0]);
            $user->setLname($name[1]);
            $user->setEmail($faker->email);
            $user->setPicture($faker->imageUrl('380', '180', 'cats'));
            $user->setCreatedAt($faker->dateTime($max = 'now', $timezone = null));

            $manager->persist($user);
        }

        $manager->flush();
    }
}
