<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190128092540 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE movie DROP FOREIGN KEY FK_1D5EF26F10DAF24A');
        $this->addSql('ALTER TABLE movie DROP FOREIGN KEY FK_1D5EF26F217BBB47');
        $this->addSql('DROP INDEX IDX_1D5EF26F217BBB47 ON movie');
        $this->addSql('DROP INDEX IDX_1D5EF26F10DAF24A ON movie');
        $this->addSql('ALTER TABLE movie DROP person_id, DROP actor_id');
        $this->addSql('ALTER TABLE actor ADD movie_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE actor ADD CONSTRAINT FK_447556F98F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id)');
        $this->addSql('CREATE INDEX IDX_447556F98F93B6FC ON actor (movie_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE actor DROP FOREIGN KEY FK_447556F98F93B6FC');
        $this->addSql('DROP INDEX IDX_447556F98F93B6FC ON actor');
        $this->addSql('ALTER TABLE actor DROP movie_id');
        $this->addSql('ALTER TABLE movie ADD person_id INT DEFAULT NULL, ADD actor_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE movie ADD CONSTRAINT FK_1D5EF26F10DAF24A FOREIGN KEY (actor_id) REFERENCES actor (id)');
        $this->addSql('ALTER TABLE movie ADD CONSTRAINT FK_1D5EF26F217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
        $this->addSql('CREATE INDEX IDX_1D5EF26F217BBB47 ON movie (person_id)');
        $this->addSql('CREATE INDEX IDX_1D5EF26F10DAF24A ON movie (actor_id)');
    }
}
