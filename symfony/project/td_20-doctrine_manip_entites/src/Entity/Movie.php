<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MovieRepository")
 */
class Movie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Person", inversedBy="movies")
     */
    private $producer;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Actor", inversedBy="movies")
     */
    private $actor;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $releaseDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Realisator", inversedBy="movie")
     */
    private $realisator;

    public function __construct()
    {
        $this->actors = new ArrayCollection();
        $this->actor = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getProducer(): ?Person
    {
        return $this->producer;
    }

    public function setProducer(?Person $producer): self
    {
        $this->producer = $producer;

        return $this;
    }

    /**
     * @return Collection|Actor[]
     */
    public function getActor(): Collection
    {
        return $this->actor;
    }

    public function addActor(Actor $actor): self
    {
        if (!$this->actor->contains($actor)) {
            $this->actor[] = $actor;
        }

        return $this;
    }

    public function removeActor(Actor $actor): self
    {
        if ($this->actor->contains($actor)) {
            $this->actor->removeElement($actor);
        }

        return $this;
    }

    public function getReleaseDate(): ?string
    {
        return $this->releaseDate;
    }

    public function setReleaseDate(string $releaseDate): self
    {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    public function getRealisator(): ?Realisator
    {
        return $this->realisator;
    }

    public function setRealisator(?Realisator $realisator): self
    {
        $this->realisator = $realisator;

        return $this;
    }
}
