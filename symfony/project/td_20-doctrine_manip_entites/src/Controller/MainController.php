<?php

namespace App\Controller;

use App\Entity\Actor;
use App\Entity\Movie;
use App\Entity\Person;
use App\Entity\Realisator;

use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MainController extends AbstractController
{
    /**
     * @Route("/movie/{id}", name="movie")
     */
    public function movies($id) {
        $repo = $this->getDoctrine()->getRepository(Movie::class);

        $actors = $repo->findOneById($id);

        return $this->render('main/movie.html.twig', [
            'actors' => $actors
        ]);
    }

    /**
     * @Route("/prodBy/{id}", name="productor")
     */
    public function productBy($id) {
        $repo = $this->getDoctrine()->getRepository(Person::class);
        $pm = $repo->findOneById($id);

        return $this->render('main/productor.html.twig', [
            'productorMovies' => $pm
        ]);
    }

    /**
     * @Route("/realizedBy/{id}", name="realisator")
     */
    public function realizedBy($id) {
        $repo = $this->getDoctrine()->getRepository(Realisator::class);
        $rm = $repo->findOneById($id);

        return $this->render('main/realisator.html.twig', [
            'reaMovies' => $rm
        ]);
    }

    /**
     * @Route("/add", name="add")
     */
    public function addMovie(ObjectManager $manager, Request $req) {
        $movie = new Movie;
        $producer = new Person;
        $actor = new Actor;
        $rea = new Realisator;

        $formMovie = $this->createFormBuilder($movie)
                     ->add('name')
                     ->add('releaseDate')
                     ->add('producer', EntityType::class, [
                        'by_reference' => false,
                        'class' => Person::class,
                        'choice_label' => 'name',
                        'required' => false
                     ])
                     ->add('producerText', TextType::class, [
                        'mapped' => false,
                        'required' => false
                     ])
                     ->add('actor', EntityType::class, [
                        'by_reference' => false,
                        'class' => Actor::class,
                        'choice_label' => 'name',
                        'required' => false,
                        'multiple' => true
                     ])
                     ->add('actorText', TextType::class, [
                        'mapped' => false,
                        'required' => false
                     ])
                    // ->add('actor', CollectionType::class, [
                    //     'entry_type' => Actor::class, // le formulaire enfant qui doit être répété
                    //     'allow_add' => false, // true si tu veux que l'utilisateur puisse en ajouter
                    //     'allow_delete' => false, // true si tu veux que l'utilisateur puisse en supprimer
                    //     'label' => 'Questions',
                    //     'by_reference' => false, // voir
                    // ])

                    //voir comment ajouter plusieurs input pour ajouter plusieurs acteurs => faire un formType apparemment 'entry_type'

                     ->add('realisator', EntityType::class, [
                        'by_reference' => false,
                        'class' => Realisator::class,
                        'choice_label' => 'name',
                        'required' => false
                     ])
                     ->add('reaText', TextType::class, [
                        'mapped' => false,
                        'required' => false
                     ])
                     ->getForm();

        $formMovie->handleRequest($req);

        if ($formMovie->isSubmitted() && $formMovie->isValid()) {

            if($formMovie->get('producer')->getData() === null && $formMovie->get('producerText')->getData()) {
                $producer->setName($formMovie->get('producerText')->getData());
                $movie->setProducer($producer);
                $manager->persist($producer);
            }

            if($formMovie->get('actorText')->getData()) {
                $actor->setName($formMovie->get('actorText')->getData());
                $movie->addActor($actor);
                $manager->persist($actor);
            }

            if($formMovie->get('realisator')->getData() === null && $formMovie->get('reaText')->getData()) {
                $rea->setName($formMovie->get('reaText')->getData());
                $movie->setRealisator($rea);
                $manager->persist($rea);
            }

            $manager->persist($movie);
            $manager->flush();

            return $this->redirectToRoute('home');
        } 

        return $this->render('main/addMovie.html.twig', [
            'formMovie' => $formMovie->createView()
        ]);
    }

    /**
     * @Route("/{sort}", name="home")
     */
    public function index($sort = 'ASC') {
        $repo = $this->getDoctrine()->getRepository(Movie::class);
        $movies = $repo->FindByDate($sort);

        return $this->render('main/index.html.twig', [
            'movies' => $movies,
            'sort' => $sort
        ]);
    }
}
