<?php

namespace App\Controller;

use App\Entity\Formation;
use App\Entity\Module;

use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FormationController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index() {
        $repo = $this->getDoctrine()->getRepository(Formation::class);
        $formation = $repo->findAll();

        return $this->render('formation/index.html.twig', [
            'formations' => $formation
        ]);
    }

    /**
     * @Route("/module/{slug}", name="modules")
     */
    public function module($slug) {
        $repo = $this->getDoctrine()->getRepository(Module::class);
        $module = $repo->findBySlug($slug);

        $repo = $this->getDoctrine()->getRepository(Formation::class);
        $formation = $repo->findAll();

        // var_dump($module); die();

        return $this->render('formation/module.show.html.twig', [
            'formation' => $formation,
            'module' => $module[0]
        ]);
    }

    /**
     * @Route("/training/{slug}", name="formation_home")
     */
    public function index_formation($slug) {
        $repo = $this->getDoctrine()->getRepository(Formation::class);

        $formationList = $repo->findAll();
        $formationName = $repo->findBySlug($slug);

        return $this->render('formation/formation.index.html.twig', [
            'formation' => $formationName[0],
            'formations' => $formationList
        ]);
    }

    /**
     * @Route("/formation/new", name="add_formation")
     */
    public function newForma(Request $req, ObjectManager $manager) {
        $formation = new Formation;
        $module = new Module;

        $form = $this->createFormBuilder($formation)
                     ->add('name')
                     ->add('modules', EntityType::class, [
                        'by_reference' => false,
                         'class' => Module::class,
                         'choice_label' => 'name',
                         'multiple' => true
                     ])
                     ->getForm();

        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {

            $slug = strtolower(str_replace(' ', '_', $formation->getName()));

            $formation->setSlug($slug);
            
            $manager->persist($formation);
            $manager->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('formation/addFormation.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
