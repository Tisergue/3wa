import { Injectable } from '@angular/core';

import { Album, List } from './album';
import { ALBUM_LISTS, ALBUMS } from './mock-albums'; 
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {

  private _albums: Album[] = ALBUMS; // _ convention private et protected
  private _albumList: List[] = ALBUM_LISTS;

  sendCurrentNumberPage = new Subject<number>(); // pour mettre à jour la pagination 
  subjectAlbum = new Subject<Album>();

  constructor() { }

  getAlbums(): Album[] {

    return this._albums.sort(
      (a, b) => { return b.duration - a.duration }
    );
  }

  getAlbum(id: string): Album  {

    return this._albums.find(album => album.id === id);
  }

  // recherche d'une référence dans la liste
  getAlbumList(id: string): List {

    return this._albumList.find(list => list.id === id);
  }

  count():number{

    return this._albums.length;
  }

 paginate(start: number, end: number):Album[]{

  // utilisez la méthode slice pour la pagination
  return this._albums.sort(
    (a, b) => { return b.duration - a.duration }
  ).slice(start, end);
 }

  // méthode search
  search(word: string): Album[] {
    let re = new RegExp(word.trim(), 'g');

    // filter permet de filter un tableau avec un test dans le test ci-dessous on vérifie 
    // deux choses : 1/ que album.title.match(re) n'est pas vide si c'est le contraire alors c'est pas faux
    // et 2/ si on a trouver des titres qui matchaient/t avec la recherche
    return this._albums.filter(album => album.title.match(re) && album.title.match(re).length > 0) ;
  }

  currentPage(page: number) {
    return this.sendCurrentNumberPage.next(page);
  }

  switchOn(album: Album) {

    this._albums.forEach(
      a => {
        if (a.id === album.id) album.status = 'on';
        else
          a.status = 'off';
      }
    );

    this.subjectAlbum.next(album); // Observer puscher les informations
  }

  // Cette méthode n'a pas besoin d'émettre une information à l'Observable
  switchOff(album: Album) {
    this._albums.forEach(
      a => {
        a.status = 'off';
      }
    );
  }
  
}
