import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Routes
import { AppRoutingModule } from './app-routing.module';

// Pipe
import { ConvertTime } from './pipe/convert.pipe';

// Component
import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { LoginComponent } from './component/security/login/login.component';
import { LogoutComponent } from './component/security/logout/logout.component';
import { AlbumsComponent } from './component/albums/albums.component';
import { AlbumDetailsComponent } from './component/albums/details/album-details.component';
import { AlbumPlaylistComponent } from './component/albums/playlist/album-playlist.component';
import { SearchComponent } from './component/search/search.component';
import { PaginateComponent } from './component/paginate/paginate.component';
import { AudioPlayerComponent } from './component/audio-player/audio-player.component';
import { SectionHeaderComponent } from './component/section-header/section-header.component';
import { DashboardComponentComponent } from './component/dashboard-component/dashboard-component.component';


@NgModule({
  declarations: [
    ConvertTime,
    AppComponent,
    HomeComponent,
    LoginComponent,
    LogoutComponent,
    AlbumsComponent,
    AlbumDetailsComponent,
    AlbumPlaylistComponent,
    SearchComponent,
    PaginateComponent,
    AudioPlayerComponent,
    SectionHeaderComponent,
    DashboardComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
