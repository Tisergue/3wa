import { Component, OnInit } from '@angular/core';

import * as firebase from 'firebase';
import { config } from '../env/env'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor() {
    firebase.initializeApp(config)
  }
}
