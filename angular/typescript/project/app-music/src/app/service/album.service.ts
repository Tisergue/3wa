import { Injectable } from '@angular/core';

import { Subject } from 'rxjs';

import { Album } from '../class/album';
import * as mock from '../component/albums/data/mock-albums';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {

  subjectAlbum = new Subject<Album>()
  albums: Album[] = mock.ALBUMS
  selectedAlbum: Album

  constructor() { }

  switchOn(album: Album) {

    mock.ALBUMS.forEach(
      a => {
        if (a.id === album.id) album.status = 'on'
        else
          a.status = 'off'
      }
    )

    this.subjectAlbum.next(album)
  }

  switchOff(album: Album) {
    mock.ALBUMS.forEach(
      a => a.status = 'off'
    )
  }

  sort() {
    return mock.ALBUMS.sort((a, b) => b.duration - a.duration)
  }

  count() {
    return mock.ALBUMS.length
  }

  getAlbums() {
    return this.sort()
  }

  getAlbum(id: string) {
    return mock.ALBUMS.find(first => first.id === id)
  }

  getAlbumList(id: string) {
    return mock.ALBUM_LISTS.find(first => first.id === id)
  }

  paginate(start: number, end: number):Album[] {
    return this.sort().slice(start, end)
  }

  search(word: string): Album[] {
    let res = []

    if (word && word.length > 2) {
      this.albums.forEach(album => {
        if (album.title.includes(word)) res.push(album)
      })

      if (res.length === 0) return this.albums
      else return res
    }

    return this.albums
  }
}