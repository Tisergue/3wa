import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';

import * as firebase from 'firebase/app';
import 'firebase/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  private authState: boolean
  public userSub = new Subject()

  constructor(
    private router: Router
  ) { 
    firebase.auth().onAuthStateChanged( (user) => {
      if (user) {
        this.authState = true
        this.authenticated()
      }
      else this.authState = false
    })
  }

  auth(email: string, password: string): Promise<any> {
    return firebase.auth().signInWithEmailAndPassword(email, password)
  }

  authenticated() {
    this.userSub.next(this.authState)

    return this.authState
  }

  signout() {
    firebase.auth().signOut()
  }
}
