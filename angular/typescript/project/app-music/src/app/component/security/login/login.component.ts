import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router';

import { AuthServiceService } from 'src/app/service/auth-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  formLog: FormGroup
  error: string = null

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthServiceService,
    private router: Router
    
  ) { }

  ngOnInit() {
    this.formLog = this.formBuilder.group({
      email: ['', Validators.required],
      pwd: ['', Validators.required]
    })
  }

  onSubmit() {
    this.error = null

    let email = this.formLog.controls.email.value
    let pwd = this.formLog.controls.pwd.value

    this.auth.auth(email, pwd)
      .then(succes => this.router.navigateByUrl('/dashBoard'))
      .catch(error => {
        let errorMessage = error.message

        this.error = errorMessage
      })
  }

  close() {
    this.error = null

    return false
  }
}
