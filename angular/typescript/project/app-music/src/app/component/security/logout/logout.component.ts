import { Component, OnInit } from '@angular/core';

import * as firebase from 'firebase/app'

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  signout() {
    firebase.auth().signOut()
  }

}
