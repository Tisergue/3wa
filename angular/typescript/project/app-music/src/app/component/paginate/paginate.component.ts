import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import * as _ from 'lodash'

import { AlbumService } from 'src/app/service/album.service';

@Component({
  selector: 'app-paginate',
  templateUrl: './paginate.component.html',
  styleUrls: ['./paginate.component.scss']
})
export class PaginateComponent implements OnInit {

  total: number
  pages: number[] = []
  current: number
  maxPage: number

  @Output() setPaginate: EventEmitter<{ start: number; end: number }> = new EventEmitter()

  constructor(private albumService: AlbumService) { }

  ngOnInit() {
    this.initPagination()
  }

  initPagination() {
    this.total = this.albumService.count()
    this.maxPage = Math.ceil(this.total / 3)
    this.pages = _.range(1, this.maxPage + 1)
  }

  selectedPage(page: number) {
    this.current = page
    this.setPaginate.emit(this.paginate(page));

  }

  paginate(page: number): { start: number, end: number } {
    let start = (page - 1) * 3
    let end = start + 3

    return { start: start, end: end };
  }

}
