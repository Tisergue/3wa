import { Component, OnInit, EventEmitter, Output, NgModule } from '@angular/core';
import { NgForm } from '@angular/forms';

import { AlbumService } from 'src/app/service/album.service';

import { Album } from 'src/app/class/album';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  @Output() searchAlbums: EventEmitter<Album[]> = new EventEmitter()

  constructor(private albumService: AlbumService) { }

  ngOnInit() {
  }

  onSubmit(form: NgForm): void {
    const searchAlbums = this.albumService.search(form.value['word'])

    if (searchAlbums)
      this.searchAlbums.emit(searchAlbums)

    form.resetForm()
  }

}
