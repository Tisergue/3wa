import { Component, OnInit, Input } from '@angular/core';

import { AlbumService } from 'src/app/service/album.service';

import { Album } from 'src/app/class/album';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.scss']
})
export class AlbumsComponent implements OnInit {

  titlePage: string = 'Page principal Albums Music'

  albums: Album[] = this.albumService.getAlbums()
  selectedAlbum: Album
  totalAlbum: number = this.albumService.count()
  status

  check: string
  
  @Input() page: string

  constructor(private albumService: AlbumService) { }

  ngOnInit() {
    if (!this.page) this.albums = this.albumService.paginate(0, 3)
    else {
      let page  = parseInt(this.page)
      let end   = page * 2
      let start = end - 3

      this.albumService.paginate(start, end)
    }
  }

  playParent($event) {
    if (this.selectedAlbum) this.check = $event.id
    else this.check = '1'

    this.status = $event.id // identifiant unique

    // méthode dans le service
    this.albumService.switchOn($event)
  }

  onSelect(album: Album) {
    this.selectedAlbum = album
  }

  paginate($event) {
    console.log($event.start, $event.end)
    this.albums = this.albumService.paginate($event.start, $event.end)
  }

  search(event) {
    if (event) this.albums = event
  }
}
