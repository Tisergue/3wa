import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';


import { Album } from '../../../class/album';
import { Track } from '../../../class/track';

import { AlbumService } from 'src/app/service/album.service';

@Component({
  selector: 'app-album-playlist',
  templateUrl: './album-playlist.component.html',
  styleUrls: ['./album-playlist.component.scss'],
  animations: [
    trigger('myAnimation', [
      state('open', style({
        backgroundColor: '#181818'
      })),
      state('close', style({
        backgroundColor: '#787878'
      })),
      transition('open => close', [
        animate('5s')
      ])
    ])
  ]
})
export class AlbumPlaylistComponent implements OnInit {

  tracks: Track
  isOpen : boolean = false

  @Input() album: Album
  @Output() onPlay: EventEmitter<Album> = new EventEmitter()

  constructor(private albumService: AlbumService) { }

  ngOnInit() {
    this.album = this.albumService.getAlbum('1')
    this.tracks = this.albumService.getAlbumList('1')
  }

  ngOnChanges() {
    if (this.album) {
      this.tracks = this.albumService.getAlbumList(this.album.id)
    }

    this.isOpen = false

    const animate = setInterval(() => {
      this.isOpen = ! this.isOpen
      clearInterval(animate)
    }, 2000);
  }

  play(album: Album) {
    this.onPlay.emit(album) // émettre un album vers le parent
  }

  isActive(event) {
    let trackList = document.querySelectorAll('.track')
    let element = event.target

    trackList.forEach(track => {
      track.classList.remove('active')
    })
    element.className += ' active'
  }

}
