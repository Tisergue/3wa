import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { AlbumService } from 'src/app/service/album.service';

import { Album } from '../../../class/album';

@Component({
  selector: 'app-album-details',
  templateUrl: './album-details.component.html',
  styleUrls: ['./album-details.component.scss']
})
export class AlbumDetailsComponent implements OnInit {

  item: Album

  constructor(private route: ActivatedRoute, private albumService: AlbumService) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id')

    this.item = this.albumService.getAlbum(id)
  }
}
