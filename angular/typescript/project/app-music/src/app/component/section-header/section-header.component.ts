import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { interval, Subscription } from 'rxjs';

import { AuthServiceService } from 'src/app/service/auth-service.service';
import { GuardService } from 'src/app/service/guard.service';

@Component({
  selector: 'app-section-header',
  templateUrl: './section-header.component.html',
  styleUrls: ['./section-header.component.scss']
})
export class SectionHeaderComponent implements OnInit {

  title: string = 'App Music'
  count: string
  isLogged: Boolean
  userSub: Subscription
  
  constructor(
    private authService: AuthServiceService,
    private guardService: GuardService,
    private router: Router
  ) {}

  ngOnInit() {
    // this.getInterval()
    this.authService.userSub.subscribe(logged => {
      if (logged) this.isLogged = true
    })
  }

  getInterval() {
    interval(1000).subscribe(time => {
      let seconds = time
      let hrs     = Math.floor(seconds / 3600)

      seconds     -= hrs * 3600

      let mnts    = Math.floor(seconds / 60)

      seconds     -= mnts * 60;
    
      this.count = 'time: ' + hrs + ' h ' + mnts + ' min ' + seconds + ' s'
    })
  }

  onSignout() {
    this.authService.signout()
    this.isLogged = false
    this.router.navigateByUrl('/')
  }

}
