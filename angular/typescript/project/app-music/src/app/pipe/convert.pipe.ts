import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'convertTime' })
export class ConvertTime implements PipeTransform {
    transform(value: string): string {
        let converted = Math.floor(parseInt(value) / 60)
        return converted.toString()
    }
}