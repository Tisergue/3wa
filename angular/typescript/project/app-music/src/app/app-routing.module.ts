import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GuardService } from './service/guard.service';

import { HomeComponent } from './component/home/home.component';
import { LoginComponent } from './component/security/login/login.component';
import { LogoutComponent } from './component/security/logout/logout.component';
import { AlbumsComponent } from './component/albums/albums.component';
import { AlbumDetailsComponent } from './component/albums/details/album-details.component';
import { DashboardComponentComponent } from './component/dashboard-component/dashboard-component.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'page/:page', component: HomeComponent },
  { path: 'dashBoard', canActivate: [GuardService], component: DashboardComponentComponent },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'album', component: AlbumsComponent },
  { path: 'album/detail/:id', component: AlbumDetailsComponent}
]

@NgModule({
  imports: [RouterModule.forRoot(
    routes,
    // { 
    //   enableTracing: true
    // }
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
