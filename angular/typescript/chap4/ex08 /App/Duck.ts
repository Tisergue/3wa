function Feature(config) {
    return (target) => {
       Object.defineProperty(
           target.prototype,                    // Nom de la classe
           'swim',                              // Function que l'on ajoute
           { value: () => config.action }       // descripteur, définit ici la méthode elle même
       ) 
    }
}

@Feature({
    action: 'nage comme un canard !'
})

class Duck {
    say() {
        return 'Je suis un oiseau '
    }
}

let duck = new Duck

console.log(`${duck.say()} qui ${duck.swim()}`)