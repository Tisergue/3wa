// tsc app.ts --target ES5 --module commonjs

import { Book } from './Book';

let book = new Book;

book.name = "L'île Mistérieuse";

console.log(book.name);