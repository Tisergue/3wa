// tsc --target ES2015 fichier.ts

/**
 * Exercice 2 (cf: https://e.3wa.fr/pluginfile.php/60490/mod_resource/content/1/chap1.pdf)
 * 
 */

// abstract class Music {
//     protected _instrument: string = 'nothing'

//     abstract makeSound():string

//     play(): string {
//         return "play music"
//     }
// }

// class Guitar extends Music {

//     constructor() {
//         super()
//         this._instrument = 'guitar folk'
//     }
    
//     get instrument(): string {
//         return this._instrument
//     }

//     makeSound(): string {
//         return super.play()
//     }
// }

// let guitar = new Guitar

// console.log(guitar.instrument)
// console.log(guitar.makeSound())

/**
 * CORRECTION
 */

abstract class Music {
    protected _instrument: string = 'nothing';
    abstract makeSound():string;

    get instrument():string{
        return this._instrument;
    }

    play(): string {
        return "play music";
    }
}

// classe étendue
class Guitar extends Music{
    protected _instrument = 'guitar'; // définition du nom surcharge la valeur de la classe mère Music

    // implémentation du code utile de la méthode => contrat de la classe mère Music
    makeSound(){

        return "C D E F G A B";
    }
}

let guitar = new Guitar; 

console.log(guitar.instrument); // affichage du nom de l'instrument
console.log(guitar.makeSound()); // jouer de la musique affiche C D E F G A B


