/**
 * Exercice 3 (cf: https://e.3wa.fr/pluginfile.php/60490/mod_resource/content/1/chap1.pdf)
 * 
 */

// interface Duck {
//     name: string

//     swim(): string
// }

// class Thing implements Duck {
//     public name
//     private obj

//     constructor(obj: Duck) {
//         this.obj = obj
//         this.name = obj.name
//     }

//     swim() {
//         return this.obj.swim()
//     }
// }

// let obj = {
//     name: 'Canard',
//     swim() { return 'oui'}
// }

// let thing = new Thing(obj)

// console.log(`Un ${thing.name} peut-il nager ? ${thing.swim()}`)

/**
 * CORRECTION
 */

// définition de l'interface <=> contrat
interface Duck{
    name : string ;
    swim(): string; 
}

class Thing implements Duck{
    name: string;    
    
    swim(): string {
       return "Nage comme un canard";
    }
}

let duck = new Thing;
console.log(duck.swim());