/* La liste des employés (nom, prénom et fonction) et des bureaux (adresse et ville) dans lequel ils travaillent */
SELECT lastName, firstName, jobTitle, addressLine1, addressLine2, city
FROM employees
INNER JOIN offices ON offices.officeCode = employees.officeCode

/* RESULTAT ==> 23 lignes / Diane Murphy */



/* La liste des clients français ou américains (nom du client, nom, prénom du contact et pays) et de leur commercial dédié (nom et prénom) triés par nom et prénom du contact */
SELECT customerName, contactLastName, contactFirstName, country, lastName, firstName
FROM customers
INNER JOIN employees ON employees.employeeNumber = customers.salesRepEmployeeNumber
WHERE country IN ('France', 'USA')
ORDER BY contactLastName, contactFirstName

/* RESULTAT ==> 48 lignes / Miguel Barajas */



/* La liste des lignes de commande (numéro de commande, code, nom et ligne de produit) et la remise appliquée aux voitures ou motos commandées triées par numéro de commande puis par remise décroissante */
SELECT orderNumber, orderdetails.productCode, productName, productLine, (MSRP - priceEach) AS discount
FROM orderdetails
INNER JOIN products ON products.productCode = orderdetails.productCode
WHERE productLine IN ('Classic Cars', 'Vintage Cars', 'Motorcycles')
ORDER BY orderNumber, discount DESC

/* RESULTAT ==> 2026 lignes / 34 */



/* La liste des salariés travaillant en amérique ou en france (nom, prénom) et des commandes que leurs clients ont passé en 2003 (numéro de commande, mois de la commande, code du produit, montant total de la ligne de commande) */

SELECT employees.firstName, employees.lastName, orderdetails.orderNumber, MONTH(orders.orderDate) AS moisCommande, orderdetails.productCode, (orderdetails.quantityOrdered * orderdetails.priceEach) AS montantTotal
FROM employees
INNER JOIN offices ON offices.officeCode = employees.officeCode
INNER JOIN customers ON customers.salesRepEmployeeNumber = employees.employeeNumber
INNER JOIN orders ON orders.customerNumber = customers.customerNumber
INNER JOIN orderdetails ON orderdetails.orderNumber = orders.orderNumber
WHERE offices.country IN ('France', 'USA') AND YEAR( orderDate ) = 2003

/* RESULTAT ==> 690 lignes / 2554.44 */