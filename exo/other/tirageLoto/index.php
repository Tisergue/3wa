<?php
    class TirageLoto {
        public function getTirage() {
            $tab = [];

            for ($i = 0; $i < 6; $i++) {
                $number = rand(1, 49);
                $tab[$i] = $number;
            }
            return $tab;
        }
    }

    $tirage = new TirageLoto();

    foreach ($tirage->getTirage() as $nb) {
        echo $nb." ";
    }

    // Pour la version courte !
    // <?= join('-', array_rand(range(1, 49), 6)); 