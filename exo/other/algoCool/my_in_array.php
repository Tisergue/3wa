<?php

    function my_in_array($needle, $haystack, bool $strict = false):bool {
        foreach ($haystack as $elem) {
            if (($strict === false && $needle == $elem) || $needle === $elem) {
                return true;
            }
        }
        return false;
    }
    
    
    
    
    
    
    
    // Ne touchez pas à la suite
    // Les tests unitaires
    $tabs = [
        [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17],
        ["1",2,"3",4,"5",6,"7",8,"9",10,"11",12,"13","14",15,"16",17],
        ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17"],
        ['a','b','c', 'd']
    ];
    
    $words = [1, '1', 10, '10', '17', 17, 'a' , 'd'];
    foreach($words as $word) {
    
        foreach($tabs as $tab) {
            if(in_array($word,$tab) !== my_in_array($word,$tab) || in_array($word, $tab, true) !== my_in_array($word, $tab, true)) {
                exit('Votre fonction my_in_array ne fonctionne pas comme in_array');
            }
        }
    }
    echo "\nVotre fonction est correcte !\n\n";