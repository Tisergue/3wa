<?php


    // function rot($nb, $message, $decrypt = false) {
        
    //     $sText = '';
    //     $letters = str_split($message);
    //     foreach($letters as $letter) {
    //         $current = ord($letter);
    //         if($decrypt)
    //             $sText .= ($current == 32 ?  ' ' : chr((($current-(12+$nb))%26)+97)  );
    //         else
    //             $sText .= ($current == 32 ?  ' ' : chr((($current+$nb)%26)+97)  );
    //     }
    //     return $sText;
    // }

    // echo rot(15, 'hello world');
    // echo "\n";
    // echo rot(15, 'pmttw ewztl', true);
    // echo "\n";
    // echo rot(14, 'olssv dvysk', true);
    // echo "\n";
    // echo rot(13, 'nkrru cuxrj', true);

    for ($i = 0; $i < strlen($argv[2]); $i++) {
        $ascii = (ord($argv[2][$i]) + $argv[1]) % 26;

        if (ord($argv[2][$i]) >= 65 && ord($argv[2][$i]) <= 90) {
            $ascii += 65;
            echo chr($ascii);
        } else if (ord($argv[2][$i]) >= 97 && ord($argv[2][$i]) <= 122) {
            $ascii += 97;
            echo chr($ascii);
        } else {
            echo chr(32);
        }
    }

    echo "\n";