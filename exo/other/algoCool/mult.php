L'algorithme procède en trois étapes.

   1)  L'algorithme multiplie par deux un chiffre sur deux, en commençant par l'avant dernier 
   		et en se déplaçant de droite à gauche. Si un chiffre qui est multiplié par deux est
        plus grand que neuf (comme c'est le cas par exemple pour 8 qui devient 16), alors il 
        faut le ramener à un chiffre entre 1 et 9. 
        
        Pour cela, il y a 2 manières de faire (pour un résultat identique) :
        
       		- Soit les chiffres composant le doublement sont additionnés 
            	(pour le chiffre 8: on obtient d'abord 16 en le multipliant par 2 puis 
                 7 en sommant les chiffres composant le résultat : 1+6).
        	- Soit on lui soustrait 9 (pour le chiffre 8 : on obtient 16 en le multipliant par 2 puis 
            	 7 en soustrayant 9 au résultat).
   
    2) La somme de tous les chiffres obtenus est effectuée.
    
    
    3) Le résultat est divisé par 10. Si le reste de la division est égal à zéro, alors le nombre original est valide.



Exemple : d'un nombre valide avec cet algo : 499273987168
<br><br>

<?php
    $number = '499273987168';
    $rev = '';
    $i = strlen($number) - 1;
    $res = '';

    while ($i >= 0) {
        $rev .= $number[$i];
        $i--;
    }

    for ($j = 1; $j < strlen($number); $j += 2) {
        $mult = $number[$j] * 2;

        if ($mult > 9) {
            $nb = intval($mult) - 9;
            $res .= strval($nb);
        } else {
            $res .= $mult;
        }
    }

    echo $res." - ".intval($res) % 10;
