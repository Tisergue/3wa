<?php

use acme\{A, B, C};
use acme\Sp\D;
use rep\E;

echo 'instance de A -> '; new A();
echo 'instance de B -> '; new B();
echo 'instance de C -> '; new C();
echo 'instance de D -> '; new D();
echo 'instance de E -> '; new E();

random();
