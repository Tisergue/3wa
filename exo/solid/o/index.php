<?php

interface IType {
    public function type();
}

class Programmer implements IType {
    public function type() {
        return '<h2>Je code !</h2>';
    }
}


class Tester implements IType {
    public function type() {
        return '<h2>Je teste !</h2>';
    }
}


class ProjectManagement {
    public function process(IType $member) {
        return $member->type();
    }
}

$programmer1 = new Programmer();
$programmer2 = new Programmer();

$tester1 = new Tester();

$project = new ProjectManagement();

try {
    //echo $project->process(new class {});
    echo $project->process($programmer1);
    echo $project->process($programmer2);
    echo $project->process($tester1);
} catch (TypeError $e) {
    echo $e->getMessage();
}