<?php

    class Programmer
    {
        public function code()
        {
            return '<h2>Je code !</h2>';
        }
    }


    class Tester
    {
        public function test()
        {
            return '<h2>Je teste !</h2>';
        }
    }


    class ProjectManagement
    {
        public function process($member)
        {
            if($member instanceof Programmer)
            {
                return $member->code();
            }
            elseif($member instanceof Tester)
            {
                return $member->test();
            }

            throw new Exception('Je ne sais pas qui vous êtes !');
        }
    }