<?php

    class CustomerPersonal extends Customer {

        public function buy(OrderProcessor $orderProcessor) {
            // Est-ce que le panier est vide ?
            if($this->getOrder() == null) {
                // Oui, donc aucun achat à faire.
                return false;
            }
            // Procède au paiement de la commande.
            $orderProcessor->checkout($this->getOrder());
            
            return true;
        }
    }