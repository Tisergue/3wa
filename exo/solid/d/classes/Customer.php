<?php

    abstract class Customer implements ICustomer {
        protected $currentOrder;

        public function addProduct(IProduct $product) {
            if($this->currentOrder == null) {
                $this->currentOrder = new Order();
            }
            $this->getOrder()->addProduct($product);
        }
        public function getOrder() {
            if($this->currentOrder == null) {
                $this->currentOrder = new Order();
            }
            return $this->currentOrder;
        }
        public function removeProduct(IProduct $product) {
            if($this->currentOrder == null) {
                throw new Exception("On ne peut pas retirer un produit d'un panier vide !");
            }
            $this->getOrder()->removeProduct($product);
        }
        public function sendMail($body) {
            echo "<h2> Mail envoyé</h2>";
        }
    }