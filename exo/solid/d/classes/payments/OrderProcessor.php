<?php

    class OrderProcessor {
        private $customer;
        private $payment;

        public function __construct(ICustomer $customer, IPayment $payment) {
            $this->customer = $customer;
            $this->payment = $payment;
        }
        // 2ème param "array $paymentDetails"
        public function checkout($totalAmount) {
            echo "<h2>status: </h2>".get_class($this->customer);
            echo "<h2>article(s): </h2>";
            foreach (current((array)$totalAmount) as $article) {
                echo get_class($article)." prix: ".$this->customer->getOrder()->getTotalAmount()." <====> code barre: ".current((array)$article)."<br>";
            }
            echo "<h2>Total: </h2>".$this->payment->execute($totalAmount)."$";
        }
    }