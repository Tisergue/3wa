<?php

    interface IPayment {
        public function execute($totalAmount);
    }