<?php

    interface ICustomer {
        public function addProduct(IProduct $product);
        public function buy(OrderProcessor $orderProcessor);
        public function getOrder();
        public function removeProduct(IProduct $product);
        public function sendMail($body);
    }