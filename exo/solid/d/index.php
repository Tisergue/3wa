<?php

    require "vendor/autoload.php";

    $customer1 = new CustomerPersonal();
    $customer1->addProduct(new Magazine('6464664'));
    $customer1->addProduct(new Book('4486262'));

    $customer1->buy(new OrderProcessor($customer1, new BankTransferPayment()));