<?php

    interface IBird {
        public function makeSound();
    }

    interface IFlyingBird extends IBird {
        public function fly();
    }

    class Ostrich implements IBird {
        public function makeSound() {
            echo '<h2>Bruh !</h2>';
        }
    }

    class Duck implements IFlyingBird {
        public function fly() {
            echo '<h2>Je vole !</h2>';
        }
        public function makeSound() {
            echo '<h2>Coin Coin naturel !</h2>';
        }
    }

    class BathDuck implements IBird {
        private $hasBattery;


        public function __construct($battery = null) {
            $this->giveBattery($battery);
        }

        public function giveBattery($battery) {
            if($battery !== null) {
                $this->hasBattery = true;
            }
        }

        public function makeSound() {
            if($this->hasBattery == true) {
                echo '<h2> Je suis un jouet !</h2>';
            }
        }
    }

    $birds = [ new Duck(), new Duck(), new Ostrich(), new BathDuck(true), new BathDuck() ];

    foreach($birds as $bird) {
        echo '<h1>Nouvel oiseau : '.get_class($bird).'</h1>';

        try {
            if($bird instanceof IFlyingBird) {
                $bird->makeSound();
                $bird->fly();
            } else {
                $bird->makeSound();
            }
            echo '<hr>';
        } catch(Exception $exception) {
            echo '<h2>ERREUR : '.$exception->getMessage().'</h2>';
        }
    }