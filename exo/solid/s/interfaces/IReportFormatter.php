<?php

    interface IReportFormatter {
        public function format(IReport $report);
    }