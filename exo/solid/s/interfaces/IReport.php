<?php
 
    interface IReport {
        public function getContents();
        public function getStatistics();
        public function setTitle($title);
    }