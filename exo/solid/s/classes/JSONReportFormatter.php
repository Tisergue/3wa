<?php

    class JSONReportFormatter implements IReportFormatter {

        public function format(IReport $report) {
            return json_encode($report->getContents());
        }
    }