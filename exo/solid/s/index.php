<?php
    require 'vendor/autoload.php';

    $report = new Report('2016-04-21', 'Titre de mon rapport');
  
    echo (new HTMLReportFormatter())->format($report);
    echo '<hr>';
    echo (new JSONReportFormatter())->format($report);