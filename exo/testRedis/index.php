<!DOCTYPE html>
<html>
<head>
<script>
    function sendMsg() {
        var str = document.getElementById("input").value
        var ul = document.getElementById("msg");
        var li = document.createElement("li");
        li.appendChild(document.createTextNode(str));
        ul.appendChild(li);

        document.getElementById("input").value = ''

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "config.php", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
        xhr.send("val=" + str);
    }
</script>
</head>
<body>
    <ul id="msg"></ul>
    <input type="text" id="input">
    <input type="submit" onClick="sendMsg()">

    <a href="/testRedis/config.php">go</a>

</body>
</html>