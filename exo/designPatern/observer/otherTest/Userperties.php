<?php
require 'connection.php';
require 'iSubject.php';
require 'iObserver.php';
require 'User.php';
require 'Observer.php';

$req = $pdo->prepare("INSERT INTO users (prenom, nom, email) VALUES (:fname, :lname, :email)");
$req->execute(array("fname" => $_GET['name'], "lname" => $_GET['lastname'], "email" => $_GET['email']));

header('location: index.php');