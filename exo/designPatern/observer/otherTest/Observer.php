<?php

class Observer implements IObserver {
    public function update(ISubject $subject) {
        echo '<hr>Changement effectué <hr>';
    }
}