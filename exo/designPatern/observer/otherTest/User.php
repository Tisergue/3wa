<?php

class User implements ISubject {
    public $users;
    private $pdo;
    private $observer = [];

    public function __construct() {
        $this->pdo = new PDO('mysql:host=localhost;dbname=test', 'root', 'troiswa');
        $users = $this->pdo->query('SELECT * from users');
        $this->users = $users->fetchAll();
    }

    public function attach(IObserver $observer) {
        $this->observer[] = $observer;
    }

    public function detach(IObserver $observer) {
        foreach ($this->observer as $key => $s) {
            if ($s === $observer) {
                unset($this->observer[$key]);
            }
        }
    }

    public function notify() {
        echo '<hr>// IL Y A DE LA NOUVEAUTÉ <hr>';

        foreach ($this->observer as $val) {
            $val->update($this);
        }
    }

    public function edit($firstname, $lastname, $email, $id) {
        $users = $this->pdo->query('UPDATE users SET email="'.$email.'", nom="'.$lastname.'", prenom="'.$firstname.'" WHERE user_id='.$id);
        $users = $this->pdo->query('SELECT * from users');
        $this->users = $users->fetchAll();

        $this->notify();
    }
}