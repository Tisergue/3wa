Créons trois classes nommées  A,B,C dans trois fichiers et qui affichent respectivement
'je suis la classe A',' ....la classe B',' ....la classe C'.
Toutes les classes se trouvent dans le répertoire src mais appartiennent au namespace Acme .
Un autre fichier D sera mis en place et se trouvera dans un sous répertoire de src qui aura pour nom (nom du repertoire) Sp et affichera je suis
la classe C.
Créons aussi un fichier teste.php qui permet d'afficher les informations des classes .
Créons un fichier nommé index.php (fichier de bootstrapping) qui sera la porte d'entrée  nous permettant
de charger le fichier d'autoload .

Créons un fichier php nommé random et qui permet d'afficher des chiffres aléatoires dès l'exécution du fichier index.php
Créons enfin un repertoire nommé rep qui sera chargé automatiquement (mais pas avec le standard psr-4 ni avec un 'require'),
qui contient une classe nommé E et qui affiche  "je suis la classe E "

Nb: On utilise deux fois la fonction require et ça sera dans le fichier index.php


/************************************Pattern Observer********************************************/

- Créons un formulaire  me permettant de m'inscrire en mettant juste mon nom,prenom et email.
- Créons un deuxième formulaire qui permet de modifier les information du sujet (entité observée.
- Faisons en sorte que s'il y'a une modification au niveau de l'entité observée, tous les objets des observateurs
  puissent recevoir une notification dans leur boite mail pour dire que le nom de l'entite observé est
  désormais "ceci" .

  Nb: il faut vérifier que toutes les méthodes de l'entité observée fonctionnent .

/******************************************Correction********************************************************/

/********Connexion à la base de données***********/
class Db
{
    public static function getMysqlConnexion()
    {
        $db = new PDO('mysql:host=localhost;dbname=observer', 'phpmyadmin', 'root');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $db;
        }
}

/****** Formulaire d'inscription *****/

<form action="manager.php" method="post">
   <label id="name">Nom:</label> <input name="name" type="text" /><br>
   <label id="prenom">prenom:</label> <input name="lastname" type="text" /><br>
    <label id="email">email:</label><input name="email" type="text" /><br>
    <input name="sent" type="submit" value="envoyer" />
</form>

/*******Le manager des entités ******/

$db = new PDO('mysql:host=localhost;dbname=observer', 'phpmyadmin', 'root');
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if (isset($_POST['sent'])){
    $q = $db->prepare('INSERT INTO user(nom,prenom,email) VALUES(:nom, :prenom,:email)');

    $q->bindValue(':nom', $_POST['name']);
    $q->bindValue(':prenom', $_POST['lastname']);
    $q->bindValue(':email', $_POST['email']);

    $q->execute();
}

if (isset($_POST['sentobservee'])) {

    $db = new PDO('mysql:host=localhost;dbname=observer', 'phpmyadmin', 'root');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $resultat = $db->query('SELECT * FROM user');

    while ($donnees = $resultat->fetchObject()) {
        $observer = new Observer;
   /*     echo '<pre>';
        var_dump($donnees);
        echo '<pre>';
     */

        $observer->setNom($donnees->nom);
        $observer->setPrenom($donnees->prenom);
        $observer->setEmail($donnees->email);



        $o = new observee;

        $o->attach($observer);
        $o->setNom($_POST['name']);
    }

}

/****************les observateurs********************/

use phpDocumentor\Reflection\Types\Array_;

class Observer implements SplObserver
{
    private $nom;
    private $prenom;
    private $email;

    public function update(SplSubject $obj)
    {
       $infos =  $obj->observers[0];
  /*      echo '<pre>';
       var_dump($infos->getEmail());
       echo '</pre>';
*/
        $to      = 'mgandega@gmail.com';
        $subject = 'the subject';
        $message = 'hello';
        $headers = array(
            'From' => 'mgandega@gmail.com',
         //   'Reply-To' => 'webmaster@example.com',
            'X-Mailer' => 'PHP/' . phpversion()
        );

        mail($to, $subject, $message, $headers);

        echo $infos->getPrenom().' '.$infos->getNom().' a été notifié ! Nouvelle valeur de l\'attribut <strong>nom</strong> : ', $obj->getNom();
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }
}

/********************LE SUJET******************/
<?php
class Observee implements SplSubject
{
    // Ceci est le tableau qui va contenir tous les objets qui nous observent.
    public $observers = [];

    // Dès que cet attribut changera on notifiera les classes observatrices.
    protected $nom;

    public function attach(SplObserver $observer)
    {

        $this->observers[] = $observer;
  /*      echo '<pre>';
        print_r($this->observers);
        echo '<pre>';
*/
        return $this;
    }

    public function detach(SplObserver $observer)
    {
        if (is_int($key = array_search($observer, $this->observers, true)))
        {
            unset($this->observers[$key]);
        }
    }

    public function notify()
    {

        foreach ($this->observers as $observer)
        {
            $obs = $observer->getEmail();

           $observer->update($this);
        }
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom($nom)
    {
        $this->nom = $nom;

        $this->notify();
    }

}

