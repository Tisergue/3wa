<?php

require 'iSubject.php';
require 'iObserver.php';
require 'User.php';
require 'Observer.php';

$user = new User;
$sub = new Observer;

$user->attach($sub);

echo '<h1>Liste des utilisateurs</h1><hr>';

$i = 0;

foreach($user->users as $row) {
    echo '<span id="'.++$i.'">id = '.$i.' - '.$row['prenom'].' '.$row['nom'].' - email: '.$row['email'].'</span><br/><hr/>';
}

echo '<br>';

if (isset($_GET['id'])) {
    $user->edit($_GET['name'], $_GET['lastname'], $_GET['email'], $_GET['id']);
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <h1>Créer un utilisateur:</h1>
    <form method="GET" action="Userperties.php">
        <label for="name">prénom</label>
        <input type="text" name="name">
        <label for="lastname">nom</label>
        <input type="text" name="lastname">
        <label for="email">email</label>
        <input type="email" name="email">

        <input type="submit">
    </form>
    <hr>
    <h1>Éditer un utilisateur:</h1>
    <form method="GET" action="index.php">
        <label for="id">id</label>
        <input type="text" name="id">
        <label for="name">prénom</label>
        <input type="text" name="name">
        <label for="lastname">nom</label>
        <input type="text" name="lastname">
        <label for="email">email</label>
        <input type="email" name="email">

        <input type="submit">
    </form>
</body>
</html>