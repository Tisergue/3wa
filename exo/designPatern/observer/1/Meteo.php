<?php
    interface IObserver {
        public function update();
    }
    class Meteo implements IObserver {
        private $SubSubject;

        public function __construct($subject) {
            $this->SubSubject = $subject;
            $this->SubSubject->attach($this);
        }

        public function update() {

        }
    }