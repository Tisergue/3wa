<?php

    interface IObserver {
        public function update();
    }
    class CurrentWeatherDisplay implements IObserver {
        private $SubSubject;

        public function __construct(ISubject $subject) {
            $this->SubSubject = $subject;
            $this->SubSubject->attach($this);
        }

        public function update() {
            echo "rain a change: ".$this->SubSubject->getRain()."<br>";
        }
    }