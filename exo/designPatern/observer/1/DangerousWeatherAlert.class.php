<?php

    interface IObserver {
        public function update();
    }
    class DangerousWeatherAlert implements IObserver {
        private $SubSubject;

        public function __construct(ISubject $subject) {
            $this->SubSubject = $subject;
            $this->SubSubject->attach($this);
        }

        public function update() {
            echo "temperature a change: ".$this->SubSubject->getTemp()."<br>";
        }
    }