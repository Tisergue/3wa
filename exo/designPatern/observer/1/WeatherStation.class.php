<?php

    interface ISubject {
        public function attach(IObserver $observable);
        public function detach(IObserver $observable);
        public function notify();
    }

    class WeatherStation implements ISubject {
        private $SubObservable;

        private $currentWeatherDisplay;
        private $dangerousWeatherAlert;
        private $tomorrowWeatherDisplay;

        private $rain;
        private $temp;
        private $wind;

        public function attach(IObserver $observable) {
            $this->SubObservable[] = $observable;
        }
        public function detach(IObserver $observable) {
            $key = array_search($observable, $this->SubObservable);

            if (false !== $key) {
                unset($this->SubObservable[$key]);
            }

            echo "supprime <br>";
        }
        public function notify() {
            foreach ($this->SubObservable as $observer) {
                $observer->update();
            }
        }

        public function changeRain($rain) {
            $this->rain = $rain;
            $this->notify();
        }
        public function getRain() {
            return $this->rain;
        }
        public function changeTemp($temp) {
            $this->temp = $temp;
            $this->notify();
        }
        public function getTemp() {
            return $this->temp;
        }

        // public function getWind() {
        //     // (...) Code renvoyant la vitesse du vent.
        //     return 42;
        // }
    }