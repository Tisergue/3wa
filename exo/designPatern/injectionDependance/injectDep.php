<?php
    interface ICapsule {
        public function getCodeBarre();
    }

    class MachineTassimo extends Reader {
        private $on;
        private $reader;
        public $capsule;

        public function __construct(ICapsule $capsule = null) {
            $this->on = false;
            $this->capsule = $capsule;
            $this->reader = new Reader();
        }

        public function onOff() {
            $this->on = $this->on === false ? true : false;

            return $this;
        }

        public function start() {
            if ($this->on === true) {
                if ($this->capsule === null) {
                    echo "<h2>Il n'y a pas de capsule dans la machine !</h2>";
                } else {
                    $this->reader->readCodeBarre($this->capsule->getCodeBarre());
                    echo "<h2>La machine est allumé et a une capsule de type ". get_class($this->capsule) ." !</h2>";
                }
            } else {
                echo "<h2>La machine n'est pas allumé !</h2>";
            }
        }
    }

    class CapsuleCappucino implements ICapsule {
        public function getCodeBarre() {
            return '0256584133658422222';
        }
    }
    
    class CapsuleChocolat implements ICapsule {
        public function getCodeBarre() {
            return '0256584133658433333';
        }
    }

    class CapsuleMilk implements ICapsule {
        public function getCodeBarre() {
            return '0256584133658444444';
        }
    }

    class Reader {
        public function readCodeBarre($code) {
            echo "<h2>Le code barre est: ".$code;
        }
    }

    $boisson = new MachineTassimo(new CapsuleCappucino());
    $boisson->onOff()->start();

    echo "<hr>";

    $boisson2 = new MachineTassimo(new CapsuleChocolat());
    $boisson2->onOff()->onOff()->start();

    echo "<hr>";

    $boisson3 = new MachineTassimo();
    $boisson3->onOff()->start();