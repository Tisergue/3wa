<?php

    abstract class Commande {
        protected $description;
        protected $price; 

        public function getDescription() {
            return $this->description;
        }

        public function getPrice() {
            return $this->price;
        }
    }

    class Waffle extends Commande {
        public function __construct() {
            $this->description = 'une gauffre';
            $this->price       = 4.0;
        }
    }

    class Pancake extends Commande {
        public function __construct() {
            $this->description = 'un pancake';
            $this->price       = 2.50;
        }
    }

    abstract class DecorateurCommande extends Commande {
        protected $commande;

        public function __construct(Commande $commande) {
            $this->commande = $commande;
        }

        public function getDescription() {
            return $this->commande->getDescription().$this->description;
        }

        public function getPrice() {
            return $this->commande->getPrice() + $this->price;
        }

    }

    class Cream extends DecorateurCommande {
        protected $description;
        protected $price;
        
        public function __construct(Commande $commande) {
            parent::__construct($commande);
            $this->description = ' , supplément chantilly';
            $this->price = 0.50;
        }
    }

    class Fudge extends DecorateurCommande {
        public function __construct(Commande $commande) {
            parent::__construct($commande);
            $this->description = ' , supplément caramel';
            $this->price = 0.40;
        }
    }

    class Chocolate extends DecorateurCommande {
        public function __construct(Commande $commande) {
            parent::__construct($commande);
            $this->description = ' , supplément chocolat';
            $this->price = 0.70;
        }
    }

    $pancake = new Pancake;
    echo "<h3>Je suis ".$pancake->getDescription()."</h3><em>Coût : ".$pancake->getPrice()." €</em><hr>";

    $pancakeChoco = new Chocolate($pancake);
    echo "<h3>Je suis ".$pancakeChoco->getDescription()."</h3><em>Coût : ".$pancakeChoco->getPrice()." €</em><hr>";

    $pancakeChocoChantilly = new Cream($pancakeChoco);
    echo "<h3>Je suis ".$pancakeChocoChantilly->getDescription()."</h3><em>Coût : ".$pancakeChocoChantilly->getPrice()." €</em><hr>";

    $waffle = new Waffle;
    echo "<h3>Je suis ".$waffle->getDescription()."</h3><em>Coût : ".$waffle->getPrice()." €</em><hr>";

    $waffleFull = new Chocolate($waffle);
    $waffleFull = new Cream($waffleFull);
    $waffleFull = new Fudge($waffleFull);
    echo "<h3>Je suis ".$waffleFull->getDescription()."</h3><em>Coût : ".$waffleFull->getPrice()." €</em><hr>";