<?php

class FudgeTopping extends Topping
{
    public function getDescription()
    {
        return $this->meal->getDescription() . ', supplément caramel';
    }

    public function getPrice()
    {
        return $this->meal->getPrice() + 0.40;
    }
}