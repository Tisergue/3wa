<?php

abstract class Sweet implements IMeal
{
    protected $description;

    protected $price;


    public function getDescription()
    {
        return $this->description;
    }

    public function getPrice()
    {
        return $this->price;
    }
}