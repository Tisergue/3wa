<?php

interface IMeal
{
    public function getDescription();
    public function getPrice();
}