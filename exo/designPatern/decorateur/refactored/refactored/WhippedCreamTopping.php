<?php

class WhippedCreamTopping extends Topping
{
    public function getDescription()
    {
        return $this->meal->getDescription() . ', supplément chantilly';
    }

    public function getPrice()
    {
        return $this->meal->getPrice() + 0.50;
    }
}