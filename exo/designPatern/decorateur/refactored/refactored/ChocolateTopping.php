<?php

class ChocolateTopping extends Topping
{
    public function getDescription()
    {
        return $this->meal->getDescription() . ', supplément chocolat';
    }

    public function getPrice()
    {
        return $this->meal->getPrice() + 0.70;
    }
}