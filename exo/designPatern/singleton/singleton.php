<?php

    class Connect {
        static private $instance;

        private $pdo;

        private function __construct($host, $dbname, $user, $password) {
            try {
                // $this->pdo = new PDO('mysql:host=localhost;dbname=test', 'root', 'troiswa');
                $this->pdo = new PDO('mysql:host='.$host.';dbname='.$dbname, $user, $password);
            } catch (PDOException $e) {
                print "Erreur !: " . $e->getMessage() . "<br/>";
                die();
            }
        }

        static public function getInstance($host = 'locahost', $dbname = null, $user = 'root', $password = '') {
            if (!self::$instance) {
                return new Connect($host, $dbname, $user, $password);
            }
        }

        public function PDO() {
            return $this->pdo;
        }
    }

    $result = Connect::getInstance('localhost', 'test', 'root', 'troiswa');
    $bdd = $result->PDO();

    foreach($bdd->query('SELECT nom from user') as $row) {
        echo $row['nom'].'<br>';
    }