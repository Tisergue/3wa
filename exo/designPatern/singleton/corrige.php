Design Pattern singleton

- Classe : Connect
- attributs : $pdo, $instance
- methodes : __construct, getPdo, getInstance


<?php

class Connect {
	
    private $pdo;
    
    private static $instance;
    
    private $data;
    
    
    private function __construct($host, $dbname, $user, $password) {
    	$this->data = 0;
    	$this->pdo = new PDO('mysql:host='.$host.';dbname='.$dbname, $user, $password);
    }
    
    public function addData() {
    	
        $this->data++;
    }
    
    public function getData() {
    	
        return $this->data;
    }
    
    public function getPdo() {
    	
        return $this->pdo;
    }
    
    public static function getInstance($dbname = null, $host = 'localhost', $user = 'root', $password = '') {
       if(self::$instance === null) { 
       		if($dbname === null) throw new Exception('Le nom de la base de données est nécessaire !');
            
       		self::$instance = new Connect($host, $dbname, $user, $password);
       }
       return self::$instance;
    }
}



$o1 = Connect::getInstance('test','localhost','root','troiswa');
$o1->getPdo();
$o1->addData(); // +1
$o2 = Connect::getInstance();
$o2->getPdo();
$o2->addData(); // +1
$o3 = Connect::getInstance('test','localhost','root','troiswa');
$o3->getPdo();
$o3->addData(); // +1

$o2->getData(); // retourne 3