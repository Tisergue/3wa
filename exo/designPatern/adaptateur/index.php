<?php

    class Programmer
    {
        public function work()
        {
            return '<h2>Je code !</h2>';
        }
    }


    class Tester
    {
        public function test()
        {
            return '<h2>Je teste !</h2>';
        }
    }

    class GraphicDesigner {
        public function GraphDesign() {
            return '<h2>Je design !</h2>';
        }
    }

    class GraphicDesignerAdaptater {
        
        private $designer;
        public function __construct(GraphicDesigner $designer) {
            $this->designer = $designer;
        }
        
        public function work() {

            return $this->designer->GraphDesign();
        }

    }
    
    class TesterAdaptater {
        
        private $tester;
        public function __construct(Tester $tester) {
            $this->tester = $tester;
        }
        
        public function work() {
            return $this->tester->test();
        }

    }

    class GenericAdaptater {
        
        private $worker;
        public function __construct($worker) {
            $this->worker = $worker;
        }
        
        public function work() {
            if ($this->worker instanceof  GraphicDesigner)
                return $this->worker->GraphDesign();
            elseif ($this->worker instanceof  Tester)
                return $this->worker->test();
        }

    }

    $workers = [new Programmer(), new GenericAdaptater(new Tester()), new GenericAdaptater(new GraphicDesigner())];

    foreach($workers as $worker) {
        echo $worker->work();
    }