<?php
    interface ISupport {
        public function getSupport();
    }

    class DVD implements ISupport {
        public function getSupport() {
            return '<h2>Vous avez lancé un DVD !</h2>';
        }
    }

    class CD implements ISupport {
        public function getSupport() {
            return '<h2>Vous avez lancé un CD !</h2>';
        }
    }

    class BlueRay implements ISupport {
        public function getSupport() {
            return '<h2>Vous avez lancé un BlueRay !</h2>';
        }
    }

    class ReaderFactory {
        public static function createSupport($type) {
            $support = null;

            switch($type) {
                case 'dvd':
                    $support = new DVD();
                    break ;
                case 'cd':
                    $support = new CD();
                    break ;
                case 'br':
                    $support = new BlueRay();
                    break ;
                default:
                    // throw new Exception ('Type de cd inconnu');
                    echo '<h2 style="color: red">ERROR: Type de cd inconnu !</h2>';
            }
            return $support;
        }
    }

    $test = ReaderFactory::createSupport('dvd');
    echo $test->getSupport();

    $test = ReaderFactory::createSupport('cd');
    echo $test->getSupport();

    $test = ReaderFactory::createSupport('br');
    echo $test->getSupport();

    $test = ReaderFactory::createSupport('test');